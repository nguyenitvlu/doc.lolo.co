FROM node:11.6.0

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json .
COPY package-lock.json .

RUN npm install

COPY ./dist ./

EXPOSE 3001
CMD ["node", "main.js"]