import {MigrationInterface, QueryRunner} from "typeorm";

export class initdb1557246199547 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`CREATE TABLE "notes" ("created_by" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_by" character varying NOT NULL, "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, "id" uuid NOT NULL DEFAULT uuid_generate_v4(), "refer_id" uuid NOT NULL, "message" character varying(1024) NOT NULL, "permissions" integer NOT NULL, "docId" uuid, CONSTRAINT "PK_af6206538ea96c4e77e9f400c3d" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "files" ("created_by" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_by" character varying NOT NULL, "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, "id" uuid NOT NULL, "refer_id" character varying, "name" character varying NOT NULL, "storage_type" character varying NOT NULL, "uri" character varying NOT NULL, "mime_type" character varying NOT NULL, "original_name" character varying NOT NULL, "abandoned" boolean NOT NULL, "size" integer, "docId" uuid, CONSTRAINT "PK_6c16b9093a142e0e7613b04a3d9" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "docs" ("created_by" character varying NOT NULL, "created_at" TIMESTAMP NOT NULL DEFAULT now(), "updated_by" character varying NOT NULL, "updated_at" TIMESTAMP NOT NULL DEFAULT now(), "version" integer NOT NULL, "approved_by" character varying NOT NULL, "approved_at" TIMESTAMP NOT NULL, "pending" text NOT NULL, "id" uuid NOT NULL DEFAULT uuid_generate_v4(), "doc_no" character varying NOT NULL, "expire_date" TIMESTAMP NOT NULL, "place_of_issue" character varying, "date_of_issue" character varying, "type" character varying NOT NULL, "statues" integer NOT NULL DEFAULT 0, "data" text, "tags" text, CONSTRAINT "PK_3a13e0daf5db0055b25d829f2f2" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "notes" ADD CONSTRAINT "FK_635c6b45e3c19235183c8b5b4de" FOREIGN KEY ("docId") REFERENCES "docs"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "files" ADD CONSTRAINT "FK_e0845e0291be18b137fda65dc42" FOREIGN KEY ("docId") REFERENCES "docs"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(`ALTER TABLE "files" DROP CONSTRAINT "FK_e0845e0291be18b137fda65dc42"`);
        await queryRunner.query(`ALTER TABLE "notes" DROP CONSTRAINT "FK_635c6b45e3c19235183c8b5b4de"`);
        await queryRunner.query(`DROP TABLE "docs"`);
        await queryRunner.query(`DROP TABLE "files"`);
        await queryRunner.query(`DROP TABLE "notes"`);
    }

}
