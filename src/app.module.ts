import { Module } from '@nestjs/common';
import * as path from 'path';
import { ConfigModule, ConfigService } from 'nestjs-config';
import { WinstonModule } from 'nest-winston';
import { transports } from 'winston';
import { TypeOrmModule } from '@nestjs/typeorm';

import { DocModule } from './storage/storage.module';

ConfigService.rootPath = path.resolve(__dirname, '..');

@Module({
  imports: [
    ConfigModule.load(path.resolve(__dirname, '**/!(*.d).config.{ts,js}')),
    WinstonModule.forRoot({
      transports: [
        new transports.Console({ level: 'info' })
      ]
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (config: ConfigService) => {
        var dbconfig = config.get('db.config');
        return {
          ...dbconfig, ... {
            migrationsRun: true,
            migrationsTableName: '__migrations',
            migrations: [
              __dirname + '/../migrations/*{.ts,.js}',
            ],
            entities: [`${__dirname}/**/*/*.entity{.ts, .js}`,],
            charset: 'utf8',
            cli: {
              "entitiesDir": __dirname + "../src",
              "migrationsDir": __dirname + "../migrations"
            }
          }
        };
      },
      inject: [ConfigService],
    }),
    DocModule
  ]
})
export class AppModule { }
