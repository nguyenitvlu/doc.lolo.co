import { Column, CreateDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';
import { Audit } from './audit';

export abstract class Approve extends Audit {

    @Column()
    approved_by: string;

    @Column()
    approved_at: Date;

    @Column('simple-json')
    pending: any
}