import { Column, CreateDateColumn, UpdateDateColumn, VersionColumn } from 'typeorm';

export abstract class Audit {

    @Column()
    created_by: string;

    @CreateDateColumn()
    created_at: Date;

    @Column()
    updated_by: string;

    @UpdateDateColumn()
    updated_at: Date;

    @VersionColumn()
    version: number;
}