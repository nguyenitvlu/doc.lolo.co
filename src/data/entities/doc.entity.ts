import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Approve } from './approve';
import { Note } from './note.entity';
import { File } from './file.entity';

@Entity('docs')
export class Document extends Approve {

    @PrimaryGeneratedColumn('uuid')
    id:string

    @Column()
    refer_id: string;

    @Column()
    refer_type: number;

    @Column({ nullable: true })
    doc_no: string;

    @Column()
    name: string;
  
    @Column({ nullable: true })
    expire_date: Date;

    @Column({ nullable: true })
    place_of_issue: string;

    @Column({ nullable: true })
    date_of_issue: string;

    /**
     * CMND, PASSPORT, DRIVER_LICENSE, VEHICLE_REG_CERT, VEHICLE_INSPECT_CERT, INSURANCE, VEHICLE_LOGO
     */
    @Column()
    type: number;

    /**
     * Bitmask values
     * bit 0: pendding
     * bit 1: reviewed
     * bit 2: approved  
     * bit 32: removed
     */
    @Column({ default: 0 })
    statues: number;

    @Column({ type: 'simple-json', nullable: true })
    data: any;

    @Column({ type: "simple-array", nullable: true })
    tags: string[]

    @OneToMany(type => File, file => file.doc, { nullable: true })
    photos: File[];

    @OneToMany(type => Note, note => note.doc, { nullable: true })
    notes: Note[]
}