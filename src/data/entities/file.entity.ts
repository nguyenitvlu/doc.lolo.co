import { Entity, Column, PrimaryColumn, ManyToOne} from 'typeorm';
import { Audit } from './audit';
import { Document } from './doc.entity';

@Entity('files')
export class File extends Audit {
  @PrimaryColumn('uuid')
  id: string;

  @Column({ nullable: true})
  refer_id: string;

  @Column({ nullable: true})
  refer_type: string;

  @Column()
  name: string;

  @Column()
  storage_type: string;

  @Column()
  uri: string;

  @Column()
  mime_type: string;

  @Column()
  original_name: string;

  @Column()
  abandoned: boolean;

  @Column({ nullable: true })
  size: number;

  @ManyToOne(type => Document, doc => doc.photos, { nullable: true })
  doc: Document

}