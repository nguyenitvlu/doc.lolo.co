import { Entity, Column, PrimaryColumn, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Audit } from './audit';
import { Document } from  './doc.entity';

@Entity('notes')
export class Note extends Audit {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('uuid')
    refer_id: string;

    @Column({ length: 1024 })
    message: string;

    /**
     * Bitmask values:
     * bit 0: admin
     * bit 1: customer
     */
    @Column()
    permissions: number;

    @ManyToOne(type => Document, doc => doc.notes)
    doc: Document
}