import { Entity, Column, PrimaryColumn, ManyToOne } from 'typeorm';
import { Document } from  './doc.entity';

@Entity('notes')
export class ProfileTemplate{

    @PrimaryColumn()
    id: number;

    @Column()
    name: string

    @Column()
    doc_type: number;
}