
import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { FirebaseModule, FirebaseAuthMiddleware } from '@lolo.vn/core';

import { Document, ProfileTemplate } from '../data/entities';
import { DocumentController } from './doc.controller';
import { DocumentService } from './doc.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Document, ProfileTemplate
    ]),
    FirebaseModule
  ],
  providers: [
    DocumentService,
  ],
  controllers: [
    DocumentController
  ],
})
export class DocModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(FirebaseAuthMiddleware)
      .forRoutes(DocumentController);
  }
}