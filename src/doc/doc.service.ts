import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Repository, In } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import * as uuid from 'uuid/v1';

import { Document, ProfileTemplate } from '../data/entities';
import { IAddProfile } from './interfaces';

@Injectable()
export class DocumentService {
  constructor(
    @InjectRepository(Document)
    private readonly _documentRepository: Repository<Document>,
    @InjectRepository(ProfileTemplate)
    private readonly _profiletemplateRepository: Repository<ProfileTemplate>

  ) { }

  /**
   * Get documentations required by a profile
   * @param {string[]} refer_ids
   * @returns {Promise<Document[]>}
   */
  async addEmptyProfile(profile: IAddProfile): Promise<any> {
    var templates = await this._profiletemplateRepository.find({ where: { id: profile.profile_type } });

    var docs = templates.map(t => ({
      refer_id: profile.refer_id,
      refer_type: profile.refer_type,
      type: t.doc_type,
      name: t.name
    } as Document));

    return this._documentRepository.insert(docs);

  }

}

