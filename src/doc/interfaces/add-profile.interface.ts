import { REFER_TYPE } from '@lolo.vn/core/const/index';
export interface IAddProfile {
    profile_type: REFER_TYPE;
    refer_id: string;
    refer_type: number;
}