export interface IUploadFile {
    id: string;
    filename: string;
    mimetype: string;
    originalname: string;
    size: number;
    storagetype: string;
    path: string;
    createdBy: string;
    updatedBy: string;
}