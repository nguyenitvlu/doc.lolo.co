import { Controller, Inject, Post, UploadedFile, UseInterceptors, OnModuleInit, UseGuards, Get, Body } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { Logger } from 'winston';
import { ApiConsumes, ApiImplicitFile } from '@nestjs/swagger';
import { AuthenticationGuard } from '@lolo.vn/core';

import { StorageService } from './storage.service';
import { IUploadFile } from './interfaces';

@Controller('storage')
export class StorageController {
  
  @Inject('winston')
  private readonly logger: Logger;

  constructor(
    private readonly storageService: StorageService
  ) {
  }

  @Post('upload')
  @UseGuards(AuthenticationGuard)
  @UseInterceptors(FileInterceptor('file'))
  @ApiConsumes('multipart/form-data')
  @ApiImplicitFile({ name: 'file', required: true})
  async upload(
    @UploadedFile() file: IUploadFile,
  ) {
    return this.storageService.upload(file);
  }
}
