
import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MulterModule } from '@nestjs/platform-express';
import MulterGoogleCloudStorage from 'multer-google-storage';
import * as uuid from 'uuid/v1';

import { FirebaseModule, FirebaseAuthMiddleware } from '@lolo.vn/core';

import { StorageService } from './storage.service';
import { StorageController } from './storage.controller';
import { File } from 'src/data/entities';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      File,
    ]),
    MulterModule.registerAsync({
      useFactory: async () => ({
        storage: new MulterGoogleCloudStorage({
          filename: (req, file, cb) => {
            file.id = uuid();
            file.storagetype = 'GSC';
            file.createdBy = req.membership_id;
            file.updatedBy = req.membership_id;
            cb(null, `/${req.uid || 'public'}/${file.id}_${file.originalname}`);
          },
          autoRetry: false,
          keyFilename: "./firebase/lolo-04f0ec7c122e.json",
          projectId: 'lolo-solution',
          bucket: 'lolo-solution.appspot.com'
        })
      })
    }),
    FirebaseModule
  ],
  providers: [
    StorageService,
  ],
  controllers: [
    StorageController
  ],
})
export class DocModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(FirebaseAuthMiddleware)
      .forRoutes(StorageController);
  }
}