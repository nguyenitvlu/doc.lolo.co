import { Injectable } from '@nestjs/common';
import { Repository, In } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

import { File } from '../data/entities';
import { IUploadFile } from './interfaces';

@Injectable()
export class StorageService {
  constructor(
    @InjectRepository(File)
    private readonly fileRepository: Repository<File>
  ) { }

  /**
  * Upsert or insert a Doc
  * @param {any} file
  * @returns {Promise<void>}
  */
  async upload(file: IUploadFile): Promise<File> {
    return await this.fileRepository.save({
      id: file.id,
      name: file.filename,
      mime_type: file.mimetype,
      original_name: file.originalname,
      size: file.size || 0,
      storage_type: file.storagetype,
      uri: file.path,
      abandoned: true,
      created_by: file.createdBy,
      updated_by: file.updatedBy
    });
  }

  /**
   * Get all documentations by using list of refer_id
   * @param {string[]} refer_ids
   * @returns {Promise<Doc[]>}
   */
  async getByListOfReferId(refer_ids: string[]): Promise<File[]> {
    return this.fileRepository.find({ where: { refer_id: In(refer_ids) } });
  }

}

